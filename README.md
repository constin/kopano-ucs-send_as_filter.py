# Kopano `send_as_filter.py` for UCS

By default Postfix on Univention Corporate Server (UCS) allows sending from an arbitrary address for authenticated users. By setting `ucr set mail /postfix/smtpd/restrictions/recipient/20 = "reject_authenticated_sender_login_mismatch"` this behaviour is changed so logged in users can only send with their own address. 

This sadly breaks functionality of `kopano-spooler` (utilised to send messages from Kopano WebApp and Z-Push) and other SMTP/IMAP configured mail clients.

Kopano is already adding additional attributes to the UCS LDAP (namely k4uUserSendAsPrivilege) to specify additional sending addresses for users. But Postfix is unaware of this setting. Which means by setting `reject_authenticated_sender_login_mismatch` you will lost the ability to use "UserSendAsPrivilege" alltogether.

==================


## `send_as_filter.py` install


`send_as_filter.py` is based on [`listfilter.py`](https://github.com/univention/univention-corporate-server/blob/4.3-3/mail/univention-mail-postfix/share/listfilter.py) which is already supplied by UCS. This script is reusing some of its parts, like for example the `/etc/listfilter.secret` file.

Therefore you shoud enable `listfilter.py` before starting to use `send_as_filter.py`

Commands

```
ucr set mail/postfix/policy/listfilter=yes
# Enable kopano-spooler (and therfore Kopano WebApp and Z-Push) to send mail unauthenticated
ucr set mail/postfix/policy/listfilter/use_sasl_username=false
/etc/init.d/postfix restart
```

Now `send_as_filter.py`

/etc/postfix/master.cf.local

```
# dc=yourdomain,dc=local is your UCS ldap domain, not mail domain
send_as_filter     unix  -       n       n       -       30       spawn user=listfilter argv=/usr/share/univention-mail-postfix/send_as_filter.py
    -b dc=yourdomain,dc=local
```

/etc/postfix/main.cf.local

```
# if something goes wrong with send_as_filter.py script will not brake postfix service
smtpd_policy_service_default_action=DUNNO
```

Commands

```
ucr set mail/postfix/smtpd/restrictions/recipient/20="check_policy_service unix:private/send_as_filter"
univention-config-registry commit /etc/postfix/main.cf
univention-config-registry commit /etc/postfix/master.cf
service postfix reload

```
If you want allow some of the users to send email from every address add them to the whitelist:

```
ucr set mail/send_from_any_email_users="user1 user2 user3 etc4"
```
